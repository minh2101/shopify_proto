// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.6.1
// source: payment/info.proto

package payment

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PaymentInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                           uint64            `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	PaymentCode                  string            `protobuf:"bytes,2,opt,name=paymentCode,proto3" json:"paymentCode,omitempty"`
	PaymentName                  string            `protobuf:"bytes,3,opt,name=paymentName,proto3" json:"paymentName,omitempty"`
	ReferenceNumber              string            `protobuf:"bytes,4,opt,name=referenceNumber,proto3" json:"referenceNumber,omitempty"`
	Amount                       string            `protobuf:"bytes,6,opt,name=amount,proto3" json:"amount,omitempty"`
	Type                         string            `protobuf:"bytes,5,opt,name=type,proto3" json:"type,omitempty"`
	ProcessedAt                  string            `protobuf:"bytes,7,opt,name=processedAt,proto3" json:"processedAt,omitempty"`
	PaymentDetails               map[string]string `protobuf:"bytes,8,rep,name=paymentDetails,proto3" json:"paymentDetails,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Balance                      string            `protobuf:"bytes,9,opt,name=balance,proto3" json:"balance,omitempty"`
	Uuid                         string            `protobuf:"bytes,10,opt,name=uuid,proto3" json:"uuid,omitempty"`
	AmountBeforeTax              string            `protobuf:"bytes,11,opt,name=amountBeforeTax,proto3" json:"amountBeforeTax,omitempty"`
	Status                       string            `protobuf:"bytes,12,opt,name=status,proto3" json:"status,omitempty"`
	Currency                     string            `protobuf:"bytes,13,opt,name=currency,proto3" json:"currency,omitempty"`
	PresentmentCurrency          string            `protobuf:"bytes,14,opt,name=presentmentCurrency,proto3" json:"presentmentCurrency,omitempty"`
	CurrencyExchangeRate         string            `protobuf:"bytes,15,opt,name=currencyExchangeRate,proto3" json:"currencyExchangeRate,omitempty"`
	Note                         string            `protobuf:"bytes,16,opt,name=note,proto3" json:"note,omitempty"`
	GiftCardAmount               string            `protobuf:"bytes,17,opt,name=giftCardAmount,proto3" json:"giftCardAmount,omitempty"`
	GiftCardCode                 string            `protobuf:"bytes,18,opt,name=giftCardCode,proto3" json:"giftCardCode,omitempty"`
	GiftCardProvider             string            `protobuf:"bytes,19,opt,name=giftCardProvider,proto3" json:"giftCardProvider,omitempty"`
	GiftCardId                   string            `protobuf:"bytes,20,opt,name=giftCardId,proto3" json:"giftCardId,omitempty"`
	IsStoreCreditConversion      bool              `protobuf:"varint,21,opt,name=isStoreCreditConversion,proto3" json:"isStoreCreditConversion,omitempty"`
	PresentmentCurrencySymbol    string            `protobuf:"bytes,22,opt,name=presentmentCurrencySymbol,proto3" json:"presentmentCurrencySymbol,omitempty"`
	GiftCardCurrencyExchangeRate string            `protobuf:"bytes,23,opt,name=giftCardCurrencyExchangeRate,proto3" json:"giftCardCurrencyExchangeRate,omitempty"`
	OutletCurrency               string            `protobuf:"bytes,24,opt,name=outletCurrency,proto3" json:"outletCurrency,omitempty"`
	OutletCurrencyExchangeRate   string            `protobuf:"bytes,25,opt,name=outletCurrencyExchangeRate,proto3" json:"outletCurrencyExchangeRate,omitempty"`
}

func (x *PaymentInfo) Reset() {
	*x = PaymentInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_payment_info_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PaymentInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PaymentInfo) ProtoMessage() {}

func (x *PaymentInfo) ProtoReflect() protoreflect.Message {
	mi := &file_payment_info_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PaymentInfo.ProtoReflect.Descriptor instead.
func (*PaymentInfo) Descriptor() ([]byte, []int) {
	return file_payment_info_proto_rawDescGZIP(), []int{0}
}

func (x *PaymentInfo) GetId() uint64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *PaymentInfo) GetPaymentCode() string {
	if x != nil {
		return x.PaymentCode
	}
	return ""
}

func (x *PaymentInfo) GetPaymentName() string {
	if x != nil {
		return x.PaymentName
	}
	return ""
}

func (x *PaymentInfo) GetReferenceNumber() string {
	if x != nil {
		return x.ReferenceNumber
	}
	return ""
}

func (x *PaymentInfo) GetAmount() string {
	if x != nil {
		return x.Amount
	}
	return ""
}

func (x *PaymentInfo) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *PaymentInfo) GetProcessedAt() string {
	if x != nil {
		return x.ProcessedAt
	}
	return ""
}

func (x *PaymentInfo) GetPaymentDetails() map[string]string {
	if x != nil {
		return x.PaymentDetails
	}
	return nil
}

func (x *PaymentInfo) GetBalance() string {
	if x != nil {
		return x.Balance
	}
	return ""
}

func (x *PaymentInfo) GetUuid() string {
	if x != nil {
		return x.Uuid
	}
	return ""
}

func (x *PaymentInfo) GetAmountBeforeTax() string {
	if x != nil {
		return x.AmountBeforeTax
	}
	return ""
}

func (x *PaymentInfo) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *PaymentInfo) GetCurrency() string {
	if x != nil {
		return x.Currency
	}
	return ""
}

func (x *PaymentInfo) GetPresentmentCurrency() string {
	if x != nil {
		return x.PresentmentCurrency
	}
	return ""
}

func (x *PaymentInfo) GetCurrencyExchangeRate() string {
	if x != nil {
		return x.CurrencyExchangeRate
	}
	return ""
}

func (x *PaymentInfo) GetNote() string {
	if x != nil {
		return x.Note
	}
	return ""
}

func (x *PaymentInfo) GetGiftCardAmount() string {
	if x != nil {
		return x.GiftCardAmount
	}
	return ""
}

func (x *PaymentInfo) GetGiftCardCode() string {
	if x != nil {
		return x.GiftCardCode
	}
	return ""
}

func (x *PaymentInfo) GetGiftCardProvider() string {
	if x != nil {
		return x.GiftCardProvider
	}
	return ""
}

func (x *PaymentInfo) GetGiftCardId() string {
	if x != nil {
		return x.GiftCardId
	}
	return ""
}

func (x *PaymentInfo) GetIsStoreCreditConversion() bool {
	if x != nil {
		return x.IsStoreCreditConversion
	}
	return false
}

func (x *PaymentInfo) GetPresentmentCurrencySymbol() string {
	if x != nil {
		return x.PresentmentCurrencySymbol
	}
	return ""
}

func (x *PaymentInfo) GetGiftCardCurrencyExchangeRate() string {
	if x != nil {
		return x.GiftCardCurrencyExchangeRate
	}
	return ""
}

func (x *PaymentInfo) GetOutletCurrency() string {
	if x != nil {
		return x.OutletCurrency
	}
	return ""
}

func (x *PaymentInfo) GetOutletCurrencyExchangeRate() string {
	if x != nil {
		return x.OutletCurrencyExchangeRate
	}
	return ""
}

var File_payment_info_proto protoreflect.FileDescriptor

var file_payment_info_proto_rawDesc = []byte{
	0x0a, 0x12, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x2f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x1c, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xb0, 0x08, 0x0a, 0x0b,
	0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x04, 0x52, 0x02, 0x69, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x70,
	0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x43, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0b, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x20, 0x0a,
	0x0b, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0b, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x28, 0x0a, 0x0f, 0x72, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x4e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x72, 0x65, 0x66, 0x65, 0x72, 0x65,
	0x6e, 0x63, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x61, 0x6d, 0x6f,
	0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e,
	0x74, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73,
	0x65, 0x64, 0x41, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x72, 0x6f, 0x63,
	0x65, 0x73, 0x73, 0x65, 0x64, 0x41, 0x74, 0x12, 0x50, 0x0a, 0x0e, 0x70, 0x61, 0x79, 0x6d, 0x65,
	0x6e, 0x74, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x18, 0x08, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x28, 0x2e, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x2e, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e,
	0x74, 0x49, 0x6e, 0x66, 0x6f, 0x2e, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x44, 0x65, 0x74,
	0x61, 0x69, 0x6c, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0e, 0x70, 0x61, 0x79, 0x6d, 0x65,
	0x6e, 0x74, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x6c,
	0x61, 0x6e, 0x63, 0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x61, 0x6c, 0x61,
	0x6e, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x75, 0x75, 0x69, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x04, 0x75, 0x75, 0x69, 0x64, 0x12, 0x28, 0x0a, 0x0f, 0x61, 0x6d, 0x6f, 0x75, 0x6e,
	0x74, 0x42, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x54, 0x61, 0x78, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0f, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x42, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x54, 0x61,
	0x78, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x0c, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x30, 0x0a, 0x13, 0x70, 0x72, 0x65, 0x73, 0x65, 0x6e, 0x74,
	0x6d, 0x65, 0x6e, 0x74, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x0e, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x13, 0x70, 0x72, 0x65, 0x73, 0x65, 0x6e, 0x74, 0x6d, 0x65, 0x6e, 0x74, 0x43,
	0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x32, 0x0a, 0x14, 0x63, 0x75, 0x72, 0x72, 0x65,
	0x6e, 0x63, 0x79, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x18,
	0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x14, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x45,
	0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x6f, 0x74, 0x65, 0x18, 0x10, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x6f, 0x74, 0x65, 0x12,
	0x26, 0x0a, 0x0e, 0x67, 0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x41, 0x6d, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x11, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x67, 0x69, 0x66, 0x74, 0x43, 0x61, 0x72,
	0x64, 0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x22, 0x0a, 0x0c, 0x67, 0x69, 0x66, 0x74, 0x43,
	0x61, 0x72, 0x64, 0x43, 0x6f, 0x64, 0x65, 0x18, 0x12, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x67,
	0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x2a, 0x0a, 0x10, 0x67,
	0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x50, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x18,
	0x13, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x67, 0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x50,
	0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x12, 0x1e, 0x0a, 0x0a, 0x67, 0x69, 0x66, 0x74, 0x43,
	0x61, 0x72, 0x64, 0x49, 0x64, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x67, 0x69, 0x66,
	0x74, 0x43, 0x61, 0x72, 0x64, 0x49, 0x64, 0x12, 0x38, 0x0a, 0x17, 0x69, 0x73, 0x53, 0x74, 0x6f,
	0x72, 0x65, 0x43, 0x72, 0x65, 0x64, 0x69, 0x74, 0x43, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x73, 0x69,
	0x6f, 0x6e, 0x18, 0x15, 0x20, 0x01, 0x28, 0x08, 0x52, 0x17, 0x69, 0x73, 0x53, 0x74, 0x6f, 0x72,
	0x65, 0x43, 0x72, 0x65, 0x64, 0x69, 0x74, 0x43, 0x6f, 0x6e, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f,
	0x6e, 0x12, 0x3c, 0x0a, 0x19, 0x70, 0x72, 0x65, 0x73, 0x65, 0x6e, 0x74, 0x6d, 0x65, 0x6e, 0x74,
	0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x53, 0x79, 0x6d, 0x62, 0x6f, 0x6c, 0x18, 0x16,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x19, 0x70, 0x72, 0x65, 0x73, 0x65, 0x6e, 0x74, 0x6d, 0x65, 0x6e,
	0x74, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x53, 0x79, 0x6d, 0x62, 0x6f, 0x6c, 0x12,
	0x42, 0x0a, 0x1c, 0x67, 0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x43, 0x75, 0x72, 0x72, 0x65,
	0x6e, 0x63, 0x79, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x18,
	0x17, 0x20, 0x01, 0x28, 0x09, 0x52, 0x1c, 0x67, 0x69, 0x66, 0x74, 0x43, 0x61, 0x72, 0x64, 0x43,
	0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52,
	0x61, 0x74, 0x65, 0x12, 0x26, 0x0a, 0x0e, 0x6f, 0x75, 0x74, 0x6c, 0x65, 0x74, 0x43, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x18, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x6f, 0x75, 0x74,
	0x6c, 0x65, 0x74, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x3e, 0x0a, 0x1a, 0x6f,
	0x75, 0x74, 0x6c, 0x65, 0x74, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x45, 0x78, 0x63,
	0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x18, 0x19, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x1a, 0x6f, 0x75, 0x74, 0x6c, 0x65, 0x74, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x45,
	0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x1a, 0x41, 0x0a, 0x13, 0x50,
	0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x45, 0x6e, 0x74,
	0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x42, 0x2b,
	0x5a, 0x29, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d, 0x69, 0x6e,
	0x68, 0x32, 0x31, 0x30, 0x31, 0x2f, 0x73, 0x68, 0x6f, 0x70, 0x69, 0x66, 0x79, 0x5f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2f, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_payment_info_proto_rawDescOnce sync.Once
	file_payment_info_proto_rawDescData = file_payment_info_proto_rawDesc
)

func file_payment_info_proto_rawDescGZIP() []byte {
	file_payment_info_proto_rawDescOnce.Do(func() {
		file_payment_info_proto_rawDescData = protoimpl.X.CompressGZIP(file_payment_info_proto_rawDescData)
	})
	return file_payment_info_proto_rawDescData
}

var file_payment_info_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_payment_info_proto_goTypes = []interface{}{
	(*PaymentInfo)(nil), // 0: product.PaymentInfo
	nil,                 // 1: product.PaymentInfo.PaymentDetailsEntry
}
var file_payment_info_proto_depIdxs = []int32{
	1, // 0: product.PaymentInfo.paymentDetails:type_name -> product.PaymentInfo.PaymentDetailsEntry
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_payment_info_proto_init() }
func file_payment_info_proto_init() {
	if File_payment_info_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_payment_info_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PaymentInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_payment_info_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_payment_info_proto_goTypes,
		DependencyIndexes: file_payment_info_proto_depIdxs,
		MessageInfos:      file_payment_info_proto_msgTypes,
	}.Build()
	File_payment_info_proto = out.File
	file_payment_info_proto_rawDesc = nil
	file_payment_info_proto_goTypes = nil
	file_payment_info_proto_depIdxs = nil
}
