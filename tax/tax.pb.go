// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.6.1
// source: tax/tax.proto

package tax

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type TaxLine struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Price     string `protobuf:"bytes,1,opt,name=price,proto3" json:"price,omitempty"`
	Rate      string `protobuf:"bytes,2,opt,name=rate,proto3" json:"rate,omitempty"`
	Title     string `protobuf:"bytes,3,opt,name=title,proto3" json:"title,omitempty"`
	ProductId string `protobuf:"bytes,4,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	Sku       string `protobuf:"bytes,5,opt,name=sku,proto3" json:"sku,omitempty"`
}

func (x *TaxLine) Reset() {
	*x = TaxLine{}
	if protoimpl.UnsafeEnabled {
		mi := &file_tax_tax_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TaxLine) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TaxLine) ProtoMessage() {}

func (x *TaxLine) ProtoReflect() protoreflect.Message {
	mi := &file_tax_tax_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TaxLine.ProtoReflect.Descriptor instead.
func (*TaxLine) Descriptor() ([]byte, []int) {
	return file_tax_tax_proto_rawDescGZIP(), []int{0}
}

func (x *TaxLine) GetPrice() string {
	if x != nil {
		return x.Price
	}
	return ""
}

func (x *TaxLine) GetRate() string {
	if x != nil {
		return x.Rate
	}
	return ""
}

func (x *TaxLine) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *TaxLine) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *TaxLine) GetSku() string {
	if x != nil {
		return x.Sku
	}
	return ""
}

var File_tax_tax_proto protoreflect.FileDescriptor

var file_tax_tax_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x74, 0x61, 0x78, 0x2f, 0x74, 0x61, 0x78, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x07, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x1c, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x7a, 0x0a, 0x07, 0x54, 0x61, 0x78, 0x4c, 0x69, 0x6e,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x72, 0x61, 0x74, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x72, 0x61, 0x74, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x74,
	0x69, 0x74, 0x6c, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69, 0x74, 0x6c,
	0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64,
	0x12, 0x10, 0x0a, 0x03, 0x73, 0x6b, 0x75, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x73,
	0x6b, 0x75, 0x42, 0x27, 0x5a, 0x25, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d,
	0x2f, 0x6d, 0x69, 0x6e, 0x68, 0x32, 0x31, 0x30, 0x31, 0x2f, 0x73, 0x68, 0x6f, 0x70, 0x69, 0x66,
	0x79, 0x5f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x74, 0x61, 0x78, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_tax_tax_proto_rawDescOnce sync.Once
	file_tax_tax_proto_rawDescData = file_tax_tax_proto_rawDesc
)

func file_tax_tax_proto_rawDescGZIP() []byte {
	file_tax_tax_proto_rawDescOnce.Do(func() {
		file_tax_tax_proto_rawDescData = protoimpl.X.CompressGZIP(file_tax_tax_proto_rawDescData)
	})
	return file_tax_tax_proto_rawDescData
}

var file_tax_tax_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_tax_tax_proto_goTypes = []interface{}{
	(*TaxLine)(nil), // 0: product.TaxLine
}
var file_tax_tax_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_tax_tax_proto_init() }
func file_tax_tax_proto_init() {
	if File_tax_tax_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_tax_tax_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TaxLine); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_tax_tax_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_tax_tax_proto_goTypes,
		DependencyIndexes: file_tax_tax_proto_depIdxs,
		MessageInfos:      file_tax_tax_proto_msgTypes,
	}.Build()
	File_tax_tax_proto = out.File
	file_tax_tax_proto_rawDesc = nil
	file_tax_tax_proto_goTypes = nil
	file_tax_tax_proto_depIdxs = nil
}
